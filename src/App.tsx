import React, {useState} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import NavBar from "./components/NavBar";
import Login from "./components/Login";
import Spinner from "./components/Spinner";
import Jokes from "./components/Pages/Jokes";
import Home from "./components/Pages/Home";
import Categories from './components/Pages/Categories';
import Search from './components/Pages/Search';
import JokesContextProvider from "./context/JokesContext";
import CategoriesContextProvider from "./context/CategoriesContext";


function App() {
    // #state
    const [loggedIn, setLogin] = useState(true);
    const [loading, setLoading] = useState(false);

    const login = () => {
        setLoading(true);
        setTimeout(() => {
            setLogin(true);
            setLoading(false);
        }, 2500);
    }

    return (
        <Router>
            <NavBar/>
            <div className="page">
                <JokesContextProvider>
                    <CategoriesContextProvider>
                        {/* #routing */}
                        <Switch>
                            <Route exact path="/">
                                {/* #props */}
                                {loggedIn ? <Home/> : loading ? <Spinner/> : <Login submit={login}/>}
                            </Route>
                            <Route path="/categories">
                                {loggedIn ? <Categories/> : <Redirect to="/"/>}
                            </Route>
                            <Route path="/search">
                                {loggedIn ? <Search/> : <Redirect to="/"/>}
                            </Route>
                            <Route path="/jokes">
                                {loggedIn ? <Jokes/> : <Redirect to="/"/>}
                            </Route>
                        </Switch>
                    </CategoriesContextProvider>
                </JokesContextProvider>
            </div>
        </Router>
    );
}

export default App;
