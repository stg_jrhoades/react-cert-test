import JokesContextProvider from "./JokesContext";
import Search from "../components/Pages/Search";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {CategoriesContext} from "./CategoriesContext";
import Categories from "../components/Pages/Categories";

// Switch to react-test-library
test("Search Joke function test", async () => {
    render(
        <JokesContextProvider>
            <Search />
        </JokesContextProvider>
    )

    fireEvent.change(screen.getByRole("search-input"), {target: { value: "animal" }});
    fireEvent.click(screen.getByRole("search-joke"))

    await waitFor(() => {
        expect(screen.getByRole("joke-1kXxOaLYTauxnCSVQFp-nA").textContent).toEqual("Chuck Norris has sex with whales. It is not that h");
    })

})


// Get category and category context provider
test("Categories Joke function test", async () => {
    const categoryContext = {
        categories: ["animal", "career", "celebrity", "dev", "explicit", "fashion", "food", "history", "money", "movie", "music", "political", "religion", "science", "sport", "travel"],
        fetchCategories: jest.fn(),
        loading: false,
        setLoading: jest.fn(),
    };


    render(
        <JokesContextProvider>
            <CategoriesContext.Provider value={categoryContext}>
                <Categories />
            </CategoriesContext.Provider>
        </JokesContextProvider>
    )

    fireEvent.click((screen.getByText("animal")));

    await waitFor(() => {
        expect(screen.getByRole("display-joke").textContent).toEqual(expect.anything())
    })
})