import {createContext, FunctionComponent, useState} from "react";

type Joke = {
    text: string,
    category: string,
    index: number
}


type JokesContextType = {
    jokes: Joke[],
    loading: boolean
    setLoading: Function
    addJoke: Function
    fetchJoke: Function
    searchJoke: Function
}

const JokesContextDefault: JokesContextType = {
    jokes: [],
    loading: false,
    setLoading: () => {},
    addJoke: () => {},
    fetchJoke: () => {},
    searchJoke: () => {}
};

export const JokesContext = createContext<JokesContextType>(JokesContextDefault);


// #Functional Component
const JokesContextProvider: FunctionComponent = (props ) => {
    const [jokes, setJokes] = useState<Joke[]>([]);
    const [loading, setLoading] = useState(false);

    const addJoke = (joke: Joke) => {
        joke.index = jokes.length + 1;
        // #spread operator
        setJokes([...jokes, joke]);
    }

    const fetchJoke = async (uri: string) => {
        // #promise
        return fetch(uri).then(resp => resp.json());
    }

    const searchJoke = async (query: string) => {
        setLoading(true);
        // #promise
        return  fetch(query).then(resp => resp.json());
    }


    // #jsx
    return (
        <JokesContext.Provider value={{jokes, addJoke, fetchJoke, searchJoke, loading, setLoading}}>
            {props.children}
        </JokesContext.Provider>
    )
}

export default JokesContextProvider;

