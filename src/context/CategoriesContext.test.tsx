import CategoriesContextProvider from "./CategoriesContext";
import Categories from "../components/Pages/Categories";
import {render, screen, waitFor} from "@testing-library/react";

// Switch to react-test-library
test("render test", async () => {
    const expectedCategories = ["animal", "career", "celebrity", "dev", "explicit", "fashion", "food", "history", "money", "movie", "music", "political", "religion", "science", "sport", "travel"];


    render(
        <CategoriesContextProvider>
            <Categories />
        </CategoriesContextProvider>
    )

    await waitFor(() => {
        expectedCategories.map(category => {
            expect(screen.getByText(category).textContent).toEqual(category);
        })
    })
})