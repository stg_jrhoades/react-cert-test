import {createContext, FunctionComponent, useState} from "react";

type CategoriesContextType = {
    categories: string[],
    fetchCategories: Function,
    loading: boolean,
    setLoading: Function
}

const CategoriesContextDefault: CategoriesContextType = {
    categories: [],
    fetchCategories: () => {},
    loading: true,
    setLoading: () => {}
};

export const CategoriesContext = createContext<CategoriesContextType>(CategoriesContextDefault);

// #state management
const CategoriesContextProvider: FunctionComponent = (props ) => {
    // #state management
    const [categories, setCategories] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchCategories = () => {
        fetch("https://api.chucknorris.io/jokes/categories")
            .then(resp => resp.json())
            .then(data => {
                setCategories(data);
                setLoading(false);
            })
    }


    return (
        // #state management
        <CategoriesContext.Provider value={{categories, fetchCategories, loading, setLoading}}>
            {props.children}
        </CategoriesContext.Provider>
    )
}

export default CategoriesContextProvider;