# Arrow Functions
    Filter: create a new array based on values that return true

        const newArray = array.filter((item) => { item.value > 10 });

        newArray would contain all items that have property of value that is above 10

    Reduce: runs a function to reduce the array to a single output value

        const result = array.reduce((result, currentValue) => { result * currentValue });

        result would be all the elements in the array times together
        ex: 6 = [1, 2, 3].reduce((result, currentValue) => { result * currentValue });

        Other trick would be building a sentence
        ex: "Hello World" = ["Hello", "world"].reduce((sentence, currentWord) => { `${sentence} ${currentWord}`}); 

# Blocked scope variable `Let`

`let` keyword scopes variables into the function where they are specified vs the `var` keyword which declares the variable globally

    let a = "Hello World"

    if (something true) {
        let a = 10;

        console.log(a) // return 10
    }

    console.log(a) // return Hello World
    

# Promises
`Promises` let you run code asynchronous which helps when doing time-consuming (database calls, api calls, intense cpu heavy tasks, etc.) tasks as Javascript is a single threaded language. There are 2 methods to handle Promises. Aysnc & Await or Then, Catch, Finally  

    Async & Await
        async function somethingTimeConsuming() {
            return results;
        }

        const result = await somethingTimeConsuming();

    Callback functions
        promiseFunction
            .then((resp) => { return resp }) // This runs on a success or resolve
            .catch((error) => { handleError(error) }) // This run if an error is thrown
            .finally(() => { // This runs after either then or catch })
        
    

# Class Components & State

    class Categories extends React.Component {
    constructor() {
        super();
        this.state = {categories: [], loading: true, showModal: false, joke: ""};
    }

    componentDidMount() {
        fetch("https://api.chucknorris.io/jokes/categories")
            .then(resp => resp.json())
            .then(data => {
                this.setState({
                    categories: data,
                    loading: false
                })
            })
    }

    getJoke(cat: string) {
        fetch(`https://api.chucknorris.io/jokes/random?category=${cat}`)
            .then(resp => resp.json())
            .then(data => {
                this.setState({
                    showModal: true,
                    joke: data.value
                })
            })
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        return (
            <div className="page">
              <div style={{display: this.state.loading ? "" : "none"}}>
                <Spinner/>
              </div>
              <div className="container categories"
                    style={{display: this.state.loading ? "none" : this.state.showModal ? "none" : ""}}>
                {
                   this.state.categories.map(cat => {
                      return (
                        <div onClick={() => this.getJoke(cat)} className="category" key={cat}>{cat}</div>
                      )
                   })
                }
              </div>
              {/* Modal */}
              <div style={{display: this.state.showModal ? "" : "none"}} className="modal">
                 <div className="modal-text">
                    {this.state.joke}
                 </div>
                 <button onClick={() => this.closeModal()} className="modal-button">Close</button>
              </div>
            </div>
        )
      }
    }

    export default Categories;

# Life cycle methods

    componentDidMount()

        This method runs when the component is about to render on the page as seen above it can be used to fetch data to display

    componentWillUnmount()

        This method runs when the componet is about to unmount this can be used to clean up or reset data for better optimization

    componentDidUpdate()

        This method runs when the compoennet has state update. Be careful with this one as you it'll run after every update so any changes you put in it should be wrapped in conditional to prevent infinite loop


# Hooks

    useEffect: This runs when updates occur on the functional components

        // This will run once (on mount and unmount)
        useEffect(() => { // do something }, [])

        // This will only run when someState is updated
        useEffect(() => { // do something }, [someState])

        // This will run on every update
        useEffect(() => { // do something })

        


