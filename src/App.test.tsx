import React from 'react';
import App from './App';
import {shallow, ShallowWrapper} from "enzyme";


let wrapper: ShallowWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>;
beforeEach(() => {
  wrapper = shallow(<App />);
})

test('Confirm all Pages are rendered to the App component', () => {
  expect(wrapper.find(".page").exists()).toBe(true);
  expect(wrapper.find("NavBar").exists()).toBe(true);
  expect(wrapper.find("Home").exists()).toBe(true);
  expect(wrapper.find("Categories").exists()).toBe(true);
  expect(wrapper.find("Search").exists()).toBe(true);
  expect(wrapper.find("Jokes").exists()).toBe(true);
});

test("Confirm Login and Spinner are not rendered to the App", () => {
  expect(!wrapper.find("Spinner").exists()).toBe(true);
  expect(!wrapper.find("Login").exists()).toBe(true);
})
