import {shallow, ShallowWrapper} from "enzyme";
import React from "react";
import Login from "./Login";
import {render, fireEvent} from "@testing-library/react";

// Dummy functions
const dummySubmit = jest.fn();

jest.mock('react-router-dom', () => ({
    useHistory: () => ({
        push: jest.fn()
    })
}));


// Enzyme Testing Methods
let wrapper: ShallowWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>;
beforeEach(() => {
    wrapper = shallow(<Login submit={dummySubmit} />);
});


test("Test button is disabled at render", () => {
    expect(wrapper.find(".login-form-button").prop("disabled")).toBe(true);
});

test("Test password is masked at rendered and unmasked when span M is clicked", () => {
    expect(wrapper.find({name: "password"}).prop("type")).toBe("password");
    wrapper.find("span").simulate("click")
    expect(wrapper.find({name: "password"}).prop("type")).toBe("text");
});


// React Testing Library
test("Test once email is entered button is enabled", () => {
    const { getByLabelText, getByRole } = render(<Login submit={dummySubmit} />);
    const emailInputNode = getByLabelText("Email:");


    expect(emailInputNode.value).toBe("");
    fireEvent.change(emailInputNode, {target: {value: "Testing"}});
    expect(emailInputNode.value).toBe("Testing");

    const buttonNode = getByRole("button");
    expect(buttonNode.getAttribute("disabled")).toBe("");

    fireEvent.change(emailInputNode, {target: {value: "example@example.com"}});
    expect(emailInputNode.value).toBe("example@example.com");

    expect(buttonNode.getAttribute("disabled")).toBe(null);
});

test("Bad password prevent dummySubmit from running", () => {
    const { getByLabelText, getByRole } = render(<Login submit={dummySubmit} />);
    const emailInputNode = getByLabelText("Email:");
    const passwordInputNode = getByLabelText("Password:");

    fireEvent.change(emailInputNode, {target: {value: "example@example.com"}});
    expect(emailInputNode.value).toBe("example@example.com");

    fireEvent.change(passwordInputNode, {target: {value: "badPass"}});
    expect(passwordInputNode.value).toBe("badPass");

    const buttonNode = getByRole("button");

    fireEvent.submit(buttonNode);

    expect((dummySubmit)).toHaveBeenCalledTimes(0);
});

test("Once good data is filled dummySubmit will run on submission", () => {
    const { getByLabelText, getByRole } = render(<Login submit={dummySubmit} />);
    const emailInputNode = getByLabelText("Email:");
    const passwordInputNode = getByLabelText("Password:");

    fireEvent.change(emailInputNode, {target: {value: "example@example.com"}});
    expect(emailInputNode.value).toBe("example@example.com");

    fireEvent.change(passwordInputNode, {target: {value: "Test12"}});
    expect(passwordInputNode.value).toBe("Test12");

    const buttonNode = getByRole("button");

    fireEvent.submit(buttonNode);

    expect((dummySubmit)).toHaveBeenCalledTimes(1);
});
