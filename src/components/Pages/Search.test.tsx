import React from 'react';
import {mount, shallow, ShallowWrapper} from "enzyme";
import Search from "./Search";
import {JokesContext} from "../../context/JokesContext";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";


let jokesContext: any;
beforeEach(() => {
    jokesContext = {
        jokes: [],
        loading: false,
        setLoading: jest.fn(),
        addJoke: jest.fn(),
        fetchJoke: jest.fn(),
        searchJoke: jest.fn()
    }
})

test("Test Spinner", () => {
    let wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Search />
        </JokesContext.Provider>
    )

    expect(wrapper.find("Spinner").exists()).toBe(false);

    jokesContext.loading = true;
    wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Search />
        </JokesContext.Provider>
    )

    expect(wrapper.find("Spinner").exists()).toBe(true);
})

test("jokes render", async () => {
    const joke = "the joke needs to have more than 50 characters to test that only 50 characters are displayed";
    const promise = Promise.resolve({ result: [{ id: 1, value: joke }] });
    jokesContext.searchJoke = jest.fn((uri: string) => promise)

    render(
        <JokesContext.Provider value={jokesContext}>
            <Search />
        </JokesContext.Provider>
    )

    fireEvent.change(screen.getByRole("search-input"), {target: { value: "Some joke" }});
    fireEvent.click(screen.getByRole("search-joke"))

    await waitFor(() => {
        expect(screen.getByRole("joke-1").textContent).toMatch(joke.substr(0, 50));
    })
})


test("jokes modal", async () => {
    const joke = "the joke";
    const promise = Promise.resolve({ result: [{ id: 2, value: joke }] });
    jokesContext.searchJoke = jest.fn((uri: string) => promise)

    render(
        <JokesContext.Provider value={jokesContext}>
            <Search />
        </JokesContext.Provider>
    )

    fireEvent.change(screen.getByRole("search-input"), {target: { value: "Some joke" }});
    fireEvent.click(screen.getByRole("search-joke"))

    await waitFor(() => {
        expect(screen.getByRole("joke-2").textContent).toContain(joke);
        fireEvent.click(screen.getByRole("joke-2"))
        expect(screen.getByRole("modal-text").textContent).toContain(joke);
        fireEvent.click(screen.getByRole("close-modal"));
    })
})