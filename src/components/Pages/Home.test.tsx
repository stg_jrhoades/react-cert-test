import {mount} from "enzyme";
import Home from "./Home";

jest.mock('react-router-dom', () => ({
    useHistory: () => ({
        push: jest.fn()
    })
}));

test("Page Render", () => {
    const wrapper = mount(
        <Home />
    )

    expect(wrapper.find(".container").exists()).toBe(true);
    expect(wrapper.find(".list").exists()).toBe(true);
    expect(wrapper.find(".list-item-button").exists()).toBe(true);
    expect(wrapper.find(".description").exists()).toBe(true);

    expect(wrapper.find("#home-search-button").text()).toBe("Search");
    expect(wrapper.find("#home-category-button").text()).toBe("Categories");
    expect(wrapper.find("#home-jokes-button").text()).toBe("Jokes");
})


test("Press all the buttons", () => {
    const wrapper = mount(
        <Home />
    )

    wrapper.find("#home-search-button").simulate("click");
    wrapper.find("#go-to-button").simulate("click");
    wrapper.find("#home-category-button").simulate("click");
    wrapper.find("#go-to-button").simulate("click");
    wrapper.find("#home-jokes-button").simulate("click");
    wrapper.find("#go-to-button").simulate("click");

});
