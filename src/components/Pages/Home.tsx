import {useState} from "react";
import {useHistory} from "react-router-dom";

// #arrow function
const Home = () => {
    // #const
    const history = useHistory();
    const contents = ["Search for Jokes", "Categories of Jokes", "Jokes page"];

    const [content, setContent] = useState(0);

    // #closure
    function toPage() {
        switch (content) {
            case 0:
                history.push("/search");
                break;
            case 1:
                history.push("/categories");
                break;
            case 2:
                history.push("/jokes");
                break;
            default:
                history.push("/")
        }
    }

    return (
        <div className="container">
            <div className="list">
                <button id="home-search-button" style={{backgroundColor: content === 0 ? "#61dafb" : ""}} onClick={() => setContent(0)}
                        className="list-item-button">Search
                </button>
                <button id="home-category-button" style={{backgroundColor: content === 1 ? "#61dafb" : ""}} onClick={() => setContent(1)}
                        className="list-item-button">Categories
                </button>
                <button id="home-jokes-button" style={{backgroundColor: content === 2 ? "#61dafb" : ""}} onClick={() => setContent(2)}
                        className="list-item-button">Jokes
                </button>
            </div>
            <div className="description">
                <h3>{contents[content]}</h3>
                <button id="go-to-button" onClick={() => toPage()}>Go to</button>
            </div>
        </div>
    )
}

export default Home;