import React, {useContext, useState} from "react";
import Spinner from "../Spinner";
import {JokesContext} from "../../context/JokesContext";

class Joke {
    categories: any[] = []
    created_at: string = ""
    icon_url: string = ""
    id: string = ""
    updated_at: string = ""
    url: string = ""
    value: string = ""
}

const Search = () => {
    // #hoook
    const { searchJoke, loading, setLoading } = useContext(JokesContext);
    // #hook
    const [query, setQuery] = useState("");
    const [jokes, setJokes] = useState([]);
    const [showModal, setModal] = useState(false);
    const [jk, setJk] = useState(new Joke());

    function getJokes() {
        searchJoke(`https://api.chucknorris.io/jokes/search?query=${query}`)
            .then((data: any) => {
                setJokes(data.result);
                setLoading(false);
            })
    }

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = e.target
        e.persist();
        setQuery(value)
    };

    const openModal = (joke: Joke) => {
        setJk(joke);
        setModal(true);
    }

    return (
        <div>
            {
                loading
                    ? <Spinner />
                    : <div style={{display: showModal ? "none" : ""}} className="search-bar">
                        <input
                            role="search-input"
                            className="search-bar-input"
                            type="text"
                            onChange={handleChange}
                            value={query}
                        />
                        <button
                            role="search-joke"
                            disabled={!query}
                            className="search-bar-button"
                            onClick={() => getJokes()}
                        >
                            Search
                        </button>
                    </div>
            }
            <div style={{display: showModal ? "none" : ""}} className="joke-list">
                {jokes.map((joke: Joke) => {
                    return <div role={`joke-${joke.id}`} onClick={() => openModal(joke)} className="joke-list-item"
                                key={joke.id}>{joke.value.substr(0, 50)}</div>
                })}
            </div>
            {/* Modal */}
            <div style={{display: showModal ? "" : "none"}} className="modal">
                <div role="modal-text" className="modal-text">
                    {jk.value}
                </div>
                <button role="close-modal" onClick={() => setModal(false)} className="modal-button">Close</button>
            </div>
        </div>
    )
}

export default Search;