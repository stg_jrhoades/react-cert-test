import React, {useState, useContext} from "react";
import Spinner from "../Spinner";
import {CategoriesContext} from "../../context/CategoriesContext";
import {JokesContext} from "../../context/JokesContext";


const Categories = () => {
    // #destructuring
    const { categories, fetchCategories, loading } = useContext(CategoriesContext);
    const { addJoke, fetchJoke } = useContext(JokesContext);
    // #destructuring
    const [showModal, setModal] = useState(false);
    const [joke, setJoke] = useState("")

    if (categories.length < 1) {
        fetchCategories();
    }

    // #closure #arrow function
    const getJoke = (cat: string) => {
        fetchJoke(`https://api.chucknorris.io/jokes/random?category=${cat}`)
            .then((data: any) => {
                setModal(true);
                setJoke(data.value);
                addJoke({text:data.value, category: cat});
            })
    }

    // #closure #arrow function
    const closeModal = () => {
        setModal(false);
    }

    // #jsx
    return (
            <div className="page">
                {
                    loading
                        ? <Spinner/>
                        : <div className="container categories"
                               style={{display: loading ? "none" : showModal ? "none" : ""}}>
                            {
                                // #map function
                                categories.map(cat => {
                                    return (
                                        <div id={cat} onClick={() => getJoke(cat)} className="category" key={cat}>{cat}</div>
                                    )
                                })
                            }
                        </div>
                }
                {/* Modal */}
                <div style={{display: showModal ? "" : "none"}} className="modal">
                    <div role="display-joke" className="modal-text">
                        {joke}
                    </div>
                    <button role="close-modal" onClick={() => closeModal()} className="modal-button">Close</button>
                </div>
            </div>
        )
}

export default Categories;