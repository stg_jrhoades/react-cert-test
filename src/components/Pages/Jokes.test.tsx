import {mount} from "enzyme";
import Jokes from "./Jokes";
import {JokesContext} from "../../context/JokesContext";

let jokesContext: { jokes: never[] | { text: string; category: string; index: number; }[]; loading: boolean; setLoading: Function | jest.Mock<any, any>; addJoke: Function | jest.Mock<any, any>; fetchJoke: Function | jest.Mock<any, any>; searchJoke: Function | jest.Mock<any, any>; };

const baseButton = {
    paddingTop: "5px",
    paddingBottom: "5px",
    paddingLeft: "10px",
    paddingRight: "10px",
}

const activeButtonStyle = {
    ...baseButton,
    backgroundColor: "#61dafb",
}

const nonActiveButtonStyle = {
    ...baseButton,
    backgroundColor: "grey",
    color: "white"
}

//{index: 1, category: "animal", text: "this is a joke"}
beforeEach(() => {
    jokesContext = {
        jokes: [],
        loading: false,
        setLoading: jest.fn(),
        addJoke: jest.fn(),
        fetchJoke: jest.fn(),
        searchJoke: jest.fn()
    }
})


test("Render test", () => {
    const wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Jokes />
        </JokesContext.Provider>
    )

    expect(JSON.stringify(wrapper.find("#category-button").props().style))
        .toContain(JSON.stringify(nonActiveButtonStyle));

    expect(JSON.stringify(wrapper.find("#last-viewed-button").props().style))
        .toContain(JSON.stringify(activeButtonStyle));

    expect(wrapper.find(".joke-list").exists()).toBe(true);
})


test("Check initial sort by last viewed", () => {
    jokesContext.jokes = [
        {index: 1, category: "animal", text: "this is a joke"},
        {index: 2, category: "religion", text: "this is a joke"},
    ]

    const wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Jokes />
        </JokesContext.Provider>
    )

    expect(wrapper.find(".joke-list").childAt(0).text()).toMatch("religion: this is a joke");
    expect(wrapper.find(".joke-list").childAt(1).text()).toMatch("animal: this is a joke");
})

test("Check sort changes to by category on button click", () => {
    jokesContext.jokes = [
        {index: 1, category: "animal", text: "this is a joke"},
        {index: 2, category: "religion", text: "this is a joke"},
        {index: 3, category: "history", text: "this is a joke"},
    ]

    const wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Jokes />
        </JokesContext.Provider>
    )

    const button = wrapper.find("#category-button");

    button.simulate("click");

    expect(wrapper.find(".joke-list").childAt(0).text()).toMatch("animal: this is a joke");
    expect(wrapper.find(".joke-list").childAt(1).text()).toMatch("history: this is a joke");
    expect(wrapper.find(".joke-list").childAt(2).text()).toMatch("religion: this is a joke");

    button.simulate("click")

    expect(wrapper.find(".joke-list").childAt(0).text()).toMatch("religion: this is a joke");
    expect(wrapper.find(".joke-list").childAt(1).text()).toMatch("history: this is a joke");
    expect(wrapper.find(".joke-list").childAt(2).text()).toMatch("animal: this is a joke");

})

test("Check sort reverses when index button is clicked", () => {
    jokesContext.jokes = [
        {index: 1, category: "animal", text: "this is a joke"},
        {index: 2, category: "religion", text: "this is a joke"},
        {index: 3, category: "history", text: "this is a joke"},
    ]

    const wrapper = mount(
        <JokesContext.Provider value={jokesContext}>
            <Jokes />
        </JokesContext.Provider>
    )

    const button = wrapper.find("#last-viewed-button");

    button.simulate("click");

    expect(wrapper.find(".joke-list").childAt(0).text()).toMatch("animal: this is a joke");
    expect(wrapper.find(".joke-list").childAt(1).text()).toMatch("religion: this is a joke");
    expect(wrapper.find(".joke-list").childAt(2).text()).toMatch("history: this is a joke");


    button.simulate("click");

    expect(wrapper.find(".joke-list").childAt(0).text()).toMatch("history: this is a joke");
    expect(wrapper.find(".joke-list").childAt(1).text()).toMatch("religion: this is a joke");
    expect(wrapper.find(".joke-list").childAt(2).text()).toMatch("animal: this is a joke");
})
