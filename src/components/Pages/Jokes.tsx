import {useContext, useState} from "react";
import {JokesContext} from "../../context/JokesContext";


// #Function Component
const Jokes = () => {
    const { jokes } = useContext(JokesContext);
    const [activeSort, setActiveSort] = useState("index");
    const [catArr, setCatArr] = useState(false);
    const [indexArr, setindexArr] = useState(false);

    // #styles
    const baseButton = {
        paddingTop: "5px",
        paddingBottom: "5px",
        paddingLeft: "10px",
        paddingRight: "10px",
    }

    // #styles
    const activeButtonStyle = {
        // #spread operator
        ...baseButton,
        backgroundColor: "#61dafb",
    }

    const nonActiveButtonStyle = {
        ...baseButton,
        backgroundColor: "grey",
        color: "white"
    }

    const sort = (type: string) => {
        switch (type) {
            case "category":
                setActiveSort(type);
                setCatArr(!catArr);
                break;
            case "index":
                setActiveSort(type);
                setindexArr(!indexArr);
                break;
        }
    }

    const sortIndex = () => {
        // #sort function #map function
        return jokes
            .sort((a, b) => {
                if (indexArr) {
                    return a.index - b.index
                } else {
                    return b.index - a.index
                }

            } )
            .map(joke => {
                return <div className="joke-list-item-full" key={joke.index}>{joke.category}: {joke.text}</div>
            });
    }

    const sortCategories = () => {
        return jokes
            .sort((a, b) => {
                let catA = a.category.toUpperCase();
                let catB = b.category.toUpperCase();
                if (catA < catB) {
                    if (catArr) {
                        return -1
                    } else {
                        return 1
                    }
                }
                if (catA > catB) {
                    if (catArr) {
                        return 1
                    } else {
                        return -1
                    }
                }

                return 0
            })
            .map(joke => {
                return <div className="joke-list-item-full" key={joke.index}>{joke.category}: {joke.text}</div>
            })
    }

    // #jsx
    return (
        // #render element
        <div className="joke-page">
            {/* #styles */}
            <div className="jokes-button">
                {/* #styles */}
                <button id="category-button" style={activeSort === "category" ? activeButtonStyle : nonActiveButtonStyle}
                        onClick={() => sort("category")}>
                    Categories  { catArr ? <>&uarr;</> : <>&darr;</> }
                </button>
                <button id="last-viewed-button" style={activeSort === "index" ? activeButtonStyle : nonActiveButtonStyle}
                        onClick={() => sort("index")}>
                    Time { indexArr ? <>&uarr;</> : <>&darr;</> }
                </button>
            </div>
            <div className="joke-list">
                { activeSort === "index" ?  sortIndex() : sortCategories() }
            </div>
        </div>
    )
}

export default Jokes;
