import React from 'react';
import {mount} from "enzyme";
import Categories from "./Categories";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {CategoriesContext} from "../../context/CategoriesContext";
import {JokesContext} from "../../context/JokesContext";

const expectedCategories = ["animal", "career", "celebrity", "dev", "explicit", "fashion", "food", "history", "money", "movie", "music", "political", "religion", "science", "sport", "travel"];

let categoryContext: any;
let jokesContext: any;
beforeEach(() => {
    categoryContext = {
        categories: [],
        fetchCategories: jest.fn(),
        loading: false,
        setLoading: jest.fn(),
    };

    jokesContext = {
        jokes: [],
        addJoke: jest.fn(),
        fetchJoke: jest.fn(),
    };
})

test("Spinner is being rendered only when loading state is true", () => {
    let wrapper = mount(
        <CategoriesContext.Provider value={categoryContext}>
            <Categories/>
        </CategoriesContext.Provider>
    )

    expect(wrapper.find("Spinner").exists()).toBe(false);

    categoryContext.loading = true;
    wrapper = mount(
        <CategoriesContext.Provider value={categoryContext}>
            <Categories/>
        </CategoriesContext.Provider>
    )

    expect(wrapper.find("Spinner").exists()).toBe(true);
})


test("Fetching Categories", () => {
    mount(
        <CategoriesContext.Provider value={categoryContext}>
            <Categories/>
        </CategoriesContext.Provider>
    )

    expect(categoryContext.fetchCategories).toHaveBeenCalledTimes(1);
})


test("Displays categoreis", () => {
    categoryContext.categories = expectedCategories;

    const wrapper = mount(
        <CategoriesContext.Provider value={categoryContext}>
            <Categories/>
        </CategoriesContext.Provider>
    )

    expectedCategories.map(category => {
        expect(wrapper.find(`#${category}`).text()).toContain(category);
    });
})


test("Modal and set joke", async () => {
    const promise = Promise.resolve({ value: "this is a joke" });
    categoryContext.categories = expectedCategories;
    jokesContext = {
        jokes: [],
        addJoke: jest.fn(),
        fetchJoke: jest.fn( (uri: string) => promise),
    };

    render(
        <JokesContext.Provider value={jokesContext}>
            <CategoriesContext.Provider value={categoryContext}>
                <Categories/>
            </CategoriesContext.Provider>
        </JokesContext.Provider>
    )

    fireEvent.click((screen.getByText("animal")));

    await waitFor(() => {
        expect(jokesContext.fetchJoke).toHaveBeenCalledTimes(1);
        expect(jokesContext.addJoke).toHaveBeenCalledTimes(1);
        expect(screen.getByText("this is a joke").textContent).toContain("this is a joke");
    })

    fireEvent.click(screen.getByRole("close-modal"));
})

