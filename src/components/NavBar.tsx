import { Link } from "react-router-dom";

const NavBar = () => {
    return (
        <nav className="navbar">
            <div className="links">
                {/* #routing */}
                <Link className="link" to="/">Home</Link>
                <Link className="link" to="/search">Search</Link>
                <Link className="link-title" to="/">Chuck Norris Jokes</Link>
                <Link className="link" to="/categories">Categories</Link>
                <Link className="link" to="/jokes">Jokes</Link>
            </div>
        </nav>
    )
}


export default NavBar;