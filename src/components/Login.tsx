import React, {useState} from "react";
import { useHistory } from "react-router-dom"

interface LoginProps {
    submit: Function
}

// #props
const Login = (props: LoginProps) => {
    // #hook
    const history = useHistory();
    const [cred, setCred] = useState({email: "", password: ""});
    const [valid, setValid] = useState(true);
    const [mask, setMask] = useState(true);
    const emailReg = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}");
    const passwordReg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d@$!%*?&]{6,10}$")


    // #event handling
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target
        e.persist();
        setCred({...cred, [name]: value})
    };

    function submitForm(e: React.ChangeEvent<HTMLFormElement>) {
        // Prevent page refresh
        e.preventDefault()

        // Add Validation here
        const isEmailValid: boolean = emailReg.test(cred.email);
        const isPasswordValid: boolean = passwordReg.test(cred.password)

        // Submit the data to the app component
        if (isEmailValid && isPasswordValid) {
            setValid(true);
            // #props
            props.submit(cred);
            history.push("/search")
        } else {
            setValid(false);
        }
    }

    return (
        <form onSubmit={submitForm} className="login-form">
            <label htmlFor="email">Email:</label>
            <input
                id="email"
                className="login-form-input"
                type="text"
                name="email"
                onChange={handleChange}
                value={cred.email}
            />
            <label htmlFor="password">Password:</label>
            <div style={{ display: "flex" }}>
                <input
                    id="password"
                    className="login-form-input"
                    type={ mask ? "password" : "text" }
                    name="password"
                    onChange={handleChange}
                    value={cred.password}
                />
                <span style={{ cursor: "pointer" }} onClick={ () => setMask(!mask) }> M </span>
            </div>
            <h1 style={{ display: valid ? "none" : "", color: "red" }}>
                Invalid Email or Password
            </h1>
            <button
                role="button"
                className="login-form-button"
                disabled={!emailReg.test(cred.email)}
            >
                Submit
            </button>
        </form>
    )
}


export default Login;