import Loader from "react-loader-spinner";

const Spinner = () => {
    return (
        <Loader
            type="Circles"
            color="#282c34"
        />
    )
}

export default Spinner;